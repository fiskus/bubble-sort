function $(id) {
	return document.getElementById(id);
}
function enableReplay() {
	$("replay").className = "replay";
	$("replay").onclick = function() {
		clearTimeout(n);
		clearTimeout(m);
		var bubblesContainers = document.getElementsByClassName("bubbles");
		bubblesContainers[1].innerHTML = bubblesContainers[0].innerHTML;

		var bubbles = bubblesContainers[1].getElementsByTagName("li");
		for (var k = 0; k < bubbles.length; k++) {
			bubbles[k].id = "b" + parseInt(bubbles[k].style.padding)*2/10;
		}

		list = [5, 1, 4, 2, 8, 6, 9, 3, 7];
		step = 1;
		sort();
	}
}

function subIteration(i) {
	var bubbles = document.getElementsByClassName("bubbles")[1].getElementsByTagName("li");
	for (var l = 0; l < bubbles.length; l++) {
		bubbles[l].className = "";
	}
	var a = list[i];
	var b = list[i*1 + 1];
	if (a > b) {
		var A = $("b" + a);
		var B = $("b" + b);

		list[i] = b;
		list[i*1 + 1] = a;

		swap(A, B);

		t = true;
		i++;
		n = setTimeout("subIteration('" + i + "')", 500);
		if (i >= (list.length - 1)) {
			clearTimeout(n);
		}
	} else {
		i++;
		if (i < (list.length - 1)) {
			subIteration(i);
		}
	}
}

function swap(A, B) {
	A.className = "highlighted first";
	B.className = "highlighted second";

	var tempDim = A.style.padding;
	var tempNum = A.id.substr(1);
	A.style.padding = B.style.padding;
	A.id = B.id;
	B.style.padding = tempDim;
	B.id = "b" + tempNum;
}

var step = 1;
function iteration() {
	t = false;
	var bubbles = document.getElementsByClassName("bubbles")[1].getElementsByTagName("li");
	for (var l = 0; l < bubbles.length; l++) {
		bubbles[l].innerHTML = "<i></i>" + step;
	}
	step++;

	subIteration(0);
	m = setTimeout("iteration()", 5000);
	if (t == false) {
		clearTimeout(m);
		enableReplay();
	}
}

function sort() {
	t = true;
	iteration();
}

window.onload = function() {
	list = [5, 1, 4, 2, 8, 6, 9, 3, 7];
	sort();
}

